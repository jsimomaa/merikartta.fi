import L from 'leaflet';
import 'leaflet-control-geocoder';
import 'leaflet-control-geocoder/dist/Control.Geocoder.css';
import { MapControl, withLeaflet } from 'react-leaflet';

class GeocoderControl extends MapControl {
  // @ts-ignore
  createLeafletElement(props) {
    const {
      leaflet: { map },
    } = props;
    // @ts-ignore
    const lc = L.Control.geocoder().addTo(map);
    return lc;
  }
}
export default withLeaflet(GeocoderControl);