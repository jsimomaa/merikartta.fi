import L from 'leaflet';
import { withLeaflet, MapControl } from 'react-leaflet';
import 'leaflet.locatecontrol';

class LocateControl extends MapControl {
    // @ts-ignore
  createLeafletElement(props) {
    const {
      leaflet: { map },
      ...options
    } = props;
    // @ts-ignore
    const lc = L.control.locate(options).addTo(map);
    return lc;
  }
}
export default withLeaflet(LocateControl);