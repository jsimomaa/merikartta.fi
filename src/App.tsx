import 'font-awesome/css/font-awesome.min.css';
import { LatLngTuple } from 'leaflet';
import 'leaflet.locatecontrol';
import 'leaflet.locatecontrol/dist/L.Control.Locate.min.css';
// @ts-ignore
import 'leaflet/dist/leaflet.css';
import React from 'react';
import { LayersControl, Map, TileLayer, WMSTileLayer } from 'react-leaflet';
import './App.css';
// Import the package
// @ts-ignore
import LocateControl from './LocateControl';
import GeocoderControl from './GeocoderControl';


const { BaseLayer, Overlay } = LayersControl;

const position: LatLngTuple = [60.155, 24.79];

// Setup LocateControl options
const locateOptions = {
  position: 'topleft',
  flyTo: true,
  strings: {
    title: 'Show me where I am, yo!'
  },
  markerStyle: {
    stroke: '#fff',
    strokeWidth: 3,
    fill: '#ee2a64',
    fillOpacity: 1,
    opacity: 1,
  },
  onActivate: () => { } // callback before engine starts retrieving locations
}

class MerikarttaMap extends React.PureComponent<{}> {

  private mapRef: React.RefObject<Map> = React.createRef();

  public render() {
    const osmLayer = (
      <TileLayer
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        attribution="&copy; <a target=&quot;blank_&quot; href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
      />
    );
  
    const wmsLayer = (
      <WMSTileLayer
        url="https://julkinen.vayla.fi/s57/wms"
        format="image/png"
        layers="cells"
        transparent={true}
      />
    );
    const viroWmsLayer = (
      <WMSTileLayer
        url="https://gis.vta.ee/primar/wms_ip/peeter.valing"
        format="image/png"
        layers="cells"
        transparent={true}
        attribution="<a target=&quot;blank_&quot; href=&quot;https://vayla.fi/avoindata/kayttoehdot/merikartoitusaineiston-lisenssi&quot;>Merikartoitusaineiston lisenssi</a>"
      />
    );
  
    const layerControl = (
      <LayersControl>
        <BaseLayer checked={true} name="OSM">
          {osmLayer}
        </BaseLayer>
        <Overlay checked={true} name="WMS">
          {wmsLayer}
        </Overlay>
        <Overlay checked={true} name="Viro WMS">
          {viroWmsLayer}
        </Overlay>
      </LayersControl>
    );
    // @ts-ignore
    const locateControl = <LocateControl options={locateOptions} startDirectly={false} />;
    // @ts-ignore
    const geocoderControl = <GeocoderControl />;
    return (
      <Map ref={this.mapRef} center={position} zoom={12}>
        {layerControl}
        {locateControl}
        {geocoderControl}
      </Map>
    );
  }
}

export default MerikarttaMap;

/* const wmtsLayer = (
  <WMTSTileLayer
    url="https://julkinen.liikennevirasto.fi/rasteripalvelu/wmts"
    layer="liikennevirasto:Merikarttasarja A public"
    tilematrixSet="WGS84_Pseudo-Mercator"
    format="image/png"
    transparent={true}
    opacity={1}
  />
);
 */